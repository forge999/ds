<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run()
    {
        factory(User::class)->create([
            'name' => 'Common Man',
            'email' => 'commonman@dearsarkar.com',
            'username' => 'commonman',
            'password' => bcrypt('password'),
            'type' => 3,
        ]);

        factory(User::class)->create([
            'name' => 'Indian',
            'email' => 'indian@dearsarkar.com',
            'username' => 'indian',
            'password' => bcrypt('password'),
            'type' => 3,
        ]);

        factory(User::class)->create([
            'name' => 'Vijay Pokuri',
            'email' => 'pokuri.vijay@gmail.com',
            'username' => 'pokurivijay',
            'password' => bcrypt('Dearsarkar@2020'),
            'type' => 3,
        ]);
    }
}
