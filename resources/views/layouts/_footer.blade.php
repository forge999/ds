@unless($disableFooterAds ?? false)
    <div class="container mx-auto px-4">
        @include('layouts._ads._footer')
    </div>
@endif

@include('layouts._sponsors')

<footer id="footer">
    Copyright &copy; 2015 - {{ date('Y') }} Dear Sarkar
    <span>&bull;</span>
    <a href="{{ route('terms') }}">Terms</a>
    <span>&bull;</span>
    <a href="{{ route('privacy') }}">Privacy</a>
    <span>&bull;</span>
    <a href="https://twitter.com/dearsarkar"><i class="fa fa-facebook"></i></a>
    <a href="https://www.facebook.com/dearsarkar"><i class="fa fa-twitter"></i></a>
</footer>
