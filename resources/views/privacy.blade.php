@title('Privacy Policy')

@extends('layouts.default')

@section('content')
    <div class="legal-container">
        <h1>Privacy Policy</h1>

        <p class="mb-8">
            <strong class="text-gray-500 uppercase text-xs">Last updated: January 22, 2020</strong>
        </p>

        <h3 id="1">1. Information we collect</h3>
<div class="box bg-default mvs pam type-l">To run our service and show you relevant content we need to know a little about you and your interests. Here we outline what information DearSarkar.com collects, and how we collect it.</div>
<p>When you sign or create a petition via our DearSarkar.com platform, an account is created for you; all of your activities on DearSarkar.com are then tied to this account. In the chart below, we&rsquo;ve detailed the information we may collect about you, depending on your activities on the platform, and how we obtain this information.</p>
<table class="table-bordered">
<tbody>
<tr>
<td><strong>What we collect</strong></td>
<td><strong>How we collect it</strong></td>
</tr>
<tr>
<td>Your name.</td>
<td>We require you to provide a chosen name when you create a DearSarkar.com account.</td>
</tr>
<tr>
<td>Your email address.</td>
<td>We require you to provide an email address when you create a DearSarkar.com account.</td>
</tr>
<tr>
<td>Your password.</td>
<td>We require you to enter a password when you create a DearSarkar.com account.</td>
</tr>
<tr>
<td>Your IP address. Your &ldquo;IP Address&rdquo; is a designator that is automatically assigned to the computer that you are using by your Internet Service Provider (ISP).</td>
<td>An IP Address may be identified and logged automatically in our server log files whenever you use our platform and services, along with the time of your visit and the specific page(s) that you visited.</td>
</tr>
<tr>
<td>Your postal address.</td>
<td>You may choose to provide your postal address when you create a DearSarkar.com account. Providing this information is not required to use the service.</td>
</tr>
<tr>
<td>Your telephone number.</td>
<td>You may choose to provide your telephone number when you create a DearSarkar.com account. Providing this information is not required to use the service.</td>
</tr>
<tr>
<td>Your city.</td>
<td>We use the IP service Maxmind to suggest your city from your IP address, in order to show you local petitions that may be of interest. You can change this information if it is displayed incorrectly. We do not share your information with Maxmind. Providing a city is required to use the service.</td>
</tr>
<tr>
<td>Your country.</td>
<td>As mentioned above, we use the IP service Maxmind to suggest your city from your IP address, in order to show you local petitions that may be of interest. This enables us to determine your country. We do not share your information with Maxmind. Providing a city is required to use the service.</td>
</tr>
<tr>
<td>Your profile picture.</td>
<td>You may choose to upload a profile picture when you create an account, or at any time. Providing this information is not required to use the service.</td>
</tr>
<tr>
<td>Your specific activities on or connected to the DearSarkar.com platform. These might include petitions you have started or signed, shared or promoted, or whether you decide to become a DearSarkar.com member.</td>
<td>When you are signed in or identified as particular DearSarkar.com user, your activities on or connected to the DearSarkar.com platform are automatically associated with your account.</td>
</tr>
<tr>
<td>Any other information you voluntarily submit.</td>
<td>You may be offered the choice to provide other information to us. For example, we may collect information when you respond to user surveys or provide information if we assist you by telephone. Providing this information is not required to use the service.</td>
</tr>
<tr>
<td>Your unique mobile device ID number if you access our services via a mobile application.</td>
<td>When you download and use any mobile applications we develop, we&rsquo;ll collect your unique device ID and all your account and activity information will be tied to that unique device ID. In addition, we may track and collect app usage data, such as the date and time the app on your device accesses our servers and what information and files have been downloaded to the app. This information may be associated with your account.</td>
</tr>
<tr>
<td>The name of the browser you use to access DearSarkar.com.</td>
<td>Certain information is collected by most browsers or automatically through your device, such as your Media Access Control (MAC) address, computer type (Windows PC or Mac), screen resolution, operating system name and version, device manufacturer and model, language, Internet browser type and version, and the name and version of the DearSarkar.com platform you are using. Collecting this information helps us build and deliver the best possible version of DearSarkar.com to you.</td>
</tr>
<tr>
<td>Your social media account ID, and information shared with us via your social media account.</td>
<td>We may obtain certain information through your social media accounts connected to your DearSarkar.com account, if you choose to link them. Linking a social media account is not required to access the service. For example, if you log in to DearSarkar.com via Facebook, we ask for your permission to access certain information about your Facebook account, activities and friends. Social media sites make information available to all apps through their API, such as friend lists. The information we receive depends on what information you or the social media site decide to give us.</td>
</tr>
<tr>
<td>Information inferred about the issues you care about based on your activities on the platform.</td>
<td>As part of our efforts to connect people to causes that interest them, petitions you sign might be tagged by our users or by us as particular cause areas. For example, a petition may be tagged as &ldquo;animal rights&rdquo; or &ldquo;women&rsquo;s rights&rdquo;. If you sign one petition tagged in a particular cause area, we may infer that you would be interested in other petitions tagged in the same way. We may also send you petitions that are relevant to your general geographic area.</td>
</tr>
<tr>
<td>Currently in the United States only, we use information available in public records, or other publicly available databases, such as civic data APIs which help match citisens with the elected officials who represent them at all levels in government.</td>
<td>In the United States, we receive data from the company KnowWho that contains lists of federal congresspeople, state legislators and governors. That data is integrated into our platform to enable users in the United States to accurately target the correct decision-maker for their petition. We also use this data to match petitions from a particular district to the right political representative. We do not share any user data with KnowWho. In future, we may carry out similar activities in other countries, subject to applicable law.</td>
</tr>
<tr>
<td>Information collected through cookies as disclosed in our linked Cookies Policy</td>
<td>We, and/or our service providers may use cookies, pixel tags (also known as web beacons and clear GIFs), and other similar technologies to understand user activities. Like other websites, DearSarkar.com will not function properly if cookies are not enabled. We use third-party analytics services like Google Analytics provided by Google Inc. (&ldquo;Google&rdquo;), the Amplitude service provided by Amplitude (&ldquo;Amplitude&rdquo;), the Optimizely service provided by Optimizely (&ldquo;Optimizely&rdquo;), and the Chartio service provided by Chartio (&ldquo;Chartio&rdquo;). These analytics services may use cookies and similar technologies to analyze how people use our services and provide statistical reports about aggregate user behaviour. Such services may also collect information about platform visitors&rsquo; use of other websites. Please review our&nbsp;<a class="sc-cSHVUG ZqwWI" href="https://www.DearSarkar.com/policies/cookies">Cookies Policy</a>&nbsp;for detailed information on the cookies we use, and how you can manage these.</td>
</tr>
<tr>
<td>The currency of any contributions made through DearSarkar.com.</td>
<td>We infer your currency based on your country.</td>
</tr>
<tr>
<td>The transaction amount you contributed or received through a refund or otherwise.</td>
<td>Your payment information including your credit card or bank number, expiration date, billing address and transaction amount is collected on our payment page by a third-party payment processor and will be subject to the third party&rsquo;s privacy policy. We have no control over, and are not responsible for, the third party&rsquo;s collection, use and disclosure of your personal information. We receive information on payment amount only; no other payment information is stored or saved within our systems.</td>
</tr>
</tbody>
</table>
<p>Some of the information above reveals your specific identity, or is directly tied to your specific identity, such as your name and email address. Some of this information does not reveal your specific identity, or does not directly relate to you, such as your browser and device information or information collected through cookies. If we ever combine non-personally identifiable information with personally identifiable information, the combined information will be treated by us as personally identifying information and protected accordingly.</p>
<p><strong>Our services are not directed to people under the age of sixteen (16), and we do not knowingly collect personal information from them.</strong></p>
<h3 id="2"><strong>2. How we use your information</strong></h3>
<div class="box bg-default mvs pam type-l">Here are the ways we might use your information to run our platform, provide our services and serve you better content.</div>
<p>We and our service providers may use your information for our legitimate business interests in providing a petition platform that enables people to connect with issues of interest. Our legitimate business interests are explained below, alongside examples of how your information may be used for these purposes.</p>
<table class="table-bordered">
<tbody>
<tr>
<td><strong>Purpose</strong></td>
<td><strong>Examples</strong></td>
</tr>
<tr>
<td>Providing the functionality of our platform. We engage in these activities to manage our contractual relationship with you.</td>
<td>
<ul class="list-bulleted">
<li>To send administrative information to you, for example, information regarding our services and changes to our terms, conditions, and policies.</li>
<li>To ensure that our site and apps function properly and are optimised for your computer or device and to store your preferences and settings.</li>
</ul>
</td>
</tr>
<tr>
<td>To fulfill your specific requests through the platform. We engage in these activities to manage our contractual relationship with you.</td>
<td>
<ul class="list-bulleted">
<li>To allow you to create petitions, sign petitions, join &ldquo;topics&rdquo; or &nbsp;&ldquo;movements&rdquo; (groups of similar petitions) and to follow their progress. To allow you to participate in other activities on DearSarkar.com platforms, sites and apps, as well as to complete and fulfill your transactions with us.</li>
<li>To allow you to send email messages that you choose to send to your email contacts through our platform, such as to share a petition. By using this feature, you guarantee that you have the right to use and provide us the names and email addresses you submit.</li>
<li>To facilitate the social sharing functionality that you choose to use, such as sharing content and petitions through the DearSarkar.com platform and other social media platforms like Facebook and Twitter.</li>
</ul>
</td>
</tr>
<tr>
<td>Accomplishing our business purposes. We engage in these activities to manage our contractual relationship with you, to comply with a legal obligation, because we have a legitimate interest, and/or with your consent.</td>
<td>
<ul class="list-bulleted">
<li>For data analysis, for example, to improve the efficiency of our services.</li>
<li>For audits, to verify that our internal processes function as intended and are compliant with legal, regulatory, or contractual requirements.</li>
<li>For fraud and security monitoring purposes, for example, to detect and prevent cyber-attacks or attempts to commit identity theft.</li>
<li>For developing new products, features and services.</li>
<li>For enhancing, improving or modifying our platform.</li>
<li>For identifying usage trends, for example, understanding which parts of our platform are of most interest to users.</li>
<li>For determining the effectiveness of campaigns.</li>
<li>For operating and expanding our business activities, for example, understanding which parts of our platform are of most interest to our users so we can focus our energies on meeting our users&rsquo; interests.</li>
<li>For legal compliance. In rare circumstances, we may have to use and disclose information we have about our users in order to exercise or protect legal rights or defend against legal claims under applicable law.</li>
<li>We use cookies and similar technologies detailed in our&nbsp;<a class="sc-cSHVUG ZqwWI" href="https://www.DearSarkar.com/policies/cookies">Cookies Policy</a>&nbsp;to measure and improve our services and develop features and functionalities on our platform. For example, if we see that people using a certain device use the platform the most, we might decide to build an app specifically for that device.</li>
</ul>
</td>
</tr>
<tr>
<td>Analysis of personal information for business reporting and providing personalised services. We provide personalised services either with your consent or because we have a legitimate interest.</td>
<td>
<ul class="list-bulleted">
<li>To personalise your experience by presenting petitions, campaigns and offers tailored to you based on information we have collected from you.</li>
<li>We may anonymise, de-identify and/or aggregate information and use such information to better understand and serve our users or for optimisation of our marketing and targeting efforts. For example, we may compile statistics like the percentage of our users in a state or country who care about animal rights, or the age range of those users, or to analyze the performance of particular emails.</li>
</ul>
</td>
</tr>
<tr>
<td>To share marketing communications that we believe may be of interest to you. We engage in this activity with your consent, or to manage our contractual relationship with you.</td>
<td>
<ul class="list-bulleted">
<li>Communications related to petitions you&rsquo;ve signed, other petitions that may be of interest, or petitions relevant to your location.</li>
<li>Editorial communications about specific issues or about DearSarkar.com.</li>
<li>Communications about contributions to causes or about crowdfunding for a specific petition.</li>
<li>Communications about becoming a member or subscriber of DearSarkar.com.</li>
<li>If you choose to provide your telephone number or postal address, which are not required, we may contact you by phone, SMS, or postal mail about the DearSarkar.com contribution programme or other ways you can support campaigns.</li>
<li>Invitations to DearSarkar.com events.</li>
<li>To allow you to participate in events and similar promotions and to administer these activities. Some of these activities have additional rules, which could contain additional information about how we use and disclose information about you, so we suggest that you read these rules carefully.</li>
<li>Most marketing communications will be sent via email and sometimes via social media.</li>
<li>We might remind you about particular petitions or the DearSarkar.com contribution programme, if you have have not completed starting or signing or joining.</li>
</ul>
</td>
</tr>
</tbody>
</table>
<h3 id="3"><strong>3. Who may receive your information</strong></h3>
<div class="box bg-default mvs pam type-l">Here we outline who may receive your information when it is shared either by you via the platform, or by us.</div>
<h4><strong>a. The DearSarkar.com community</strong></h4>
<ul class="list-bulleted">
<li>All information you post on our platform (such as petitions you create, reasons for signing a petition, or your posts on the DearSarkar.com Community message boards) will be visible to other users. If you choose to send messages or connect with others through our platform about petitions you have signed or shared, you disclose your personal information to the recipient of your message. Our platform provides an open forum for communication by users all around the world.&nbsp;<strong>We do not monitor, verify, or perform any background check on campaign starters, petition signers, or other users of DearSarkar.com.</strong></li>
<li>Similar to traditional paper petitions, we consider an online petition to be a public expression of support for an issue. Therefore, your name, general geographic location (i.e. city, state, country), and a link to your DearSarkar.com user profile may be displayed on the landing page for any petition you sign, and on related areas of our platform. This information will be viewable to any visitor, including the media, search engines, and other organisations that provide archival internet activities.<strong>&nbsp;If you do not wish to have your support for a petition to be public, we recommend you do not sign the petition.</strong>&nbsp;If you do not wish to have your name displayed on a petition landing page, you may select the option not to display your name and comment publicly on the petition page.</li>
<li>Your first name, last name, city and/or postcode, and the day that you signed will be shared with the person who initiated a petition you have signed, even if you select the option not to display your name and comment publicly. This is extremely important for petition starters to demonstrate the legitimacy of their signatures to the decision-makers they are working to influence.&nbsp;<strong>If you do not wish to have this information shared with the person who initiated the petition, please do not sign the petition.</strong></li>
<li>The petition starter may choose to share your name and general geographic location with the intended decision maker who is the recipient of their petition. For example, the intended decision maker may be your congressman/woman when the petition concerns an issue relevant to him or her. If you do not wish to have this information shared with the petition recipient, you should not sign the petition.</li>
<li>If you sign a petition started by an NGO or other organisation, you will be presented with the option of sharing your email address with that NGO or organisation to receive direct email updates from them (not via the platform) should you choose to provide your consent for such sharing. Such organisations are not DearSarkar.com&rsquo;s commercial partners and are in no way affiliated with DearSarkar.com. Enabling our users to interact directly with organisations, if those users consent to this connection, is part of our goal of helping people to stay informed on the causes that matter to them. We may revoke an organisation&rsquo;s access to this option in response to reports of abuse.</li>
</ul>
<h4><strong>b. Your connected social media platforms</strong></h4>
<ul class="list-bulleted">
<li>You may share your activities on DearSarkar.com with friends on other social media sites, for example, sharing a petition you signed on Facebook. To do so, you must connect your DearSarkar.com account with your social media account. In such case, you authorise us to share information with your social media account provider, and you understand that the use of the information we share will be governed by the social media site&rsquo;s privacy policy. If you do not want your information shared with other social media users or with your social media account provider, please do not connect your social media account with your DearSarkar.com account and do not use the social sharing features on the platform. For more details on how you can edit or remove the permissions you have granted to DearSarkar.com to use information from your social media accounts, please see&nbsp;<a class="sc-cSHVUG ZqwWI" href="https://www.DearSarkar.com/policies/privacy#4">Section 4c &ldquo;Your Privacy choices, social media&rdquo;</a>.</li>
<li>You may voluntarily share information on message boards, chats, profile pages, blogs, and other services to which you are able to post information and materials (including the DearSarkar.com pages on Facebook and other social media platforms). Please note that any information you post or disclose through these services will become public information, and may be available to other DearSarkar.com users, social media platform users and to the general public. We urge you to be very careful when deciding to disclose any information about yourself via the social sharing features of our platform. For more details regarding posting content to our platform, please see our&nbsp;<a class="sc-cSHVUG ZqwWI" href="https://www.DearSarkar.com/policies/terms-of-service">Terms of Service</a>.</li>
</ul>
<h4><strong>c. Our business entities and service providers</strong></h4>
<p>We may share your information with third parties for the following purposes:</p>
<ul class="list-bulleted">
<li>Companies within the&nbsp;<a class="sc-cSHVUG ZqwWI" href="https://help.DearSarkar.com/s/article/What-are-the-Change-org-group-of-companies.">DearSarkar.com group of companies</a>. We may share your information with our affiliates, which are entities under common ownership or control of DearSarkar.com, to provide our services in different countries and for the purposes described in this Privacy Policy.</li>
<li>The&nbsp;<a class="sc-cSHVUG ZqwWI" href="https://www.changefoundation.org/">DearSarkar.com Charitable Foundation</a>&nbsp;and&nbsp;<a class="sc-cSHVUG ZqwWI" href="https://changefoundation.org/about/networkpartners/">its local chapters</a>&nbsp;that operate in certain countries. Local chapters may contact you if you are within their country, as part of The DearSarkar.com Foundation&rsquo;s mission to build social movements that create transformational change.</li>
<li>Our suppliers, subcontractors and business partners (&ldquo;service providers&rdquo;):&nbsp;We may share information about you with our service providers who process information to provide services to us or on our behalf. We have contracts with our service providers that prohibit them from sharing the information about you that they collect or receive with anyone else or from using such information for other purposes.</li>
</ul>
<h4><strong>d. Legal &amp; administrative obligations</strong></h4>
<p>We may use and disclose your personal information as necessary or appropriate, especially when we have a legal obligation or legitimate interest to do so:</p>
<ul class="list-bulleted">
<li>Fraud prevention:&nbsp;We may use and disclose the information we collect from and about our users as we believe necessary to investigate, prevent, or respond to suspected illegal or fraudulent activity or to protect the safety, privacy, rights, or property of us, our users, or others.</li>
<li>Law enforcement purposes:&nbsp;If requested or required by government authorities such as law enforcement authorities, courts, regulators, or otherwise to comply with the law (which may include laws outside your country of residence), we may have to disclose information we have about our users. We also may use and disclose information collected about you in order to exercise or protect legal rights or defend against legal claims.</li>
<li>Sale or merger of our company: We have no plans to sell our business. In this unlikely event, we may use, disclose, or transfer your personal information to a third party if we or any of our company affiliates are involved in a corporate restructuring (e.g., a sale, merger, or other transfer of assets, including in connection with any bankruptcy or similar proceedings).</li>
</ul>
<h3 id="4"><strong>4. Your privacy choices</strong></h3>
<div class="box bg-default mvs pam type-l">You can edit your privacy settings and the emails you receive from DearSarkar.com at any time through our &ldquo;Privacy &amp; Preferences&rdquo; page. If you want, you can ask us for the information we have about your account and even ask us to delete it all.</div>
<h4><strong>a. Account &amp; email settings</strong></h4>
<p>When you sign or create a petition via our DearSarkar.com platform, an account and user profile page are created for you. Any petitions that you sign will not appear on your user profile by default. Any petitions that you have started and published will appear on your user profile by default. You can change your Privacy settings by clicking here&nbsp;<a class="sc-cSHVUG ZqwWI" href="https://www.DearSarkar.com/account_settings/privacy">https://www.DearSarkar.com/account_settings/privacy</a>, or logging in to your account, clicking on &ldquo;Settings,&rdquo; and selecting &ldquo;Privacy &amp; preferences&rdquo;. If you no longer want to receive marketing-related emails from DearSarkar.com going forward, you may opt out of receiving these by following the instructions contained in any such email or by logging in to your account, clicking on &ldquo;Settings,&rdquo; and selecting &ldquo;Privacy &amp; preferences.&rdquo; &nbsp;We will comply with your request(s) as soon as reasonably practicable. Please note that if you opt out of receiving marketing-related emails from us, we may still send you important administrative messages (such as updates about your account or service changes), from which you cannot opt out.</p>
<h4><strong>b. Accessing or deleting your information</strong></h4>
<p>If you would like to request to review, correct, update, suppress, or delete personal information that has been previously provided to us by you, you may log in to your account, click on &ldquo;Settings&rdquo; and update your profile information. You can also contact our Help Centre via our&nbsp;<a class="sc-cSHVUG ZqwWI" href="https://help.DearSarkar.com/s/contactsupport?language=en">contact form here</a>&nbsp;and ask us to specify what personal information we have about you and to delete certain personal information about you from our records, or request to receive an electronic copy of your personal information for purposes of transmitting it to another company (to the extent this right to data portability is provided to you by applicable law). Please let us know what information you would like us to remove from our databases or otherwise let us know what limitations you would like to put on our use of your personal information. For your protection, we may only implement requests with respect to the personal information associated with the particular email address that you use to send us your request, and we may need to verify your identity before implementing your request. We will respond to your requests consistent with applicable law, and we will try to comply with your request as soon as reasonably practicable. Please note that we may need to retain certain information for record keeping purposes and/or to complete any transactions that you began prior to your request. There may also be residual information that will remain within our databases and other records, but such residual information will no longer be tied to your identity. For example, if you created a petition, we will have records that other DearSarkar.com users signed your petition. If you subsequently ask us to delete your information from our platform and databases, information related to those other users&rsquo; signatures cannot be removed and will remain in our records.</p>
<h4><strong>c. Social media</strong></h4>
<p>You can edit or remove the permissions you have granted to DearSarkar.com to use information from your social media accounts by using your application privacy settings on your social media account If you have signed in on DearSarkar.com through Facebook connect, you are likely to have been cookied by Facebook. You can modify or change those cookies through the settings on your Facebook account. For more details on cookies, please see our&nbsp;<a class="sc-cSHVUG ZqwWI" href="https://www.DearSarkar.com/policies/cookies">Cookies Policy</a>.</p>
<h4><strong>d. Third-party analytics companies &amp; Cookies</strong></h4>
<p>We have provided details on the cookies we use and instructions for how you can opt out of these in our&nbsp;<a class="sc-cSHVUG ZqwWI" href="https://www.DearSarkar.com/policies/cookies">Cookies Policy</a>, in the section titled &ldquo;How do I manage Cookies?&rdquo;.</p>
<h3 id="5"><strong>5. Data retention &amp; security</strong></h3>
<div class="box bg-default mvs pam type-l">We take a lot of measures to protect your personal information. If you suspect someone else is using your account, let us know by contacting our&nbsp;<a class="sc-cSHVUG ZqwWI" href="https://help.DearSarkar.com/">Help Desk.</a></div>
<p>We seek to use reasonable organisational, technical, and administrative measures to protect your personal information within our organisation from loss, misuse, unauthorised access or disclosure, alteration and/or destruction. Unfortunately, no data transmission or storage system can be guaranteed to be 100% secure. If you have reason to believe that your interaction with us is no longer secure (for example, if you feel that the security of any account you might have with us has been compromised), please immediately notify our Data Protection Team using the contact details listed at the end of this&nbsp;<a class="sc-cSHVUG ZqwWI" href="https://www.DearSarkar.com/policies/privacy#8">Privacy Policy</a>.</p>
<p>We will retain your Personal Information for as long as needed or permitted in light of the purposes for which it was obtained. The criteria used to determine our retention periods include the length of time we have an ongoing relationship with you and provide our services to you, our legal obligations or whether retention is advisable in light of our legal position (such as in regard to applicable statutes of limitations, litigation or regulatory investigations).</p>
<h3 id="6"><strong>6. Cross-border transfers</strong></h3>
<div class="box bg-default mvs pam type-l">DearSarkar.com is a global organisation with offices around the world, so your information may be transferred across borders when you use the Platform. We have put in place measures to comply with laws regulating cross-border transfers.</div>
<p>DearSarkar.com is a global organisation. Your personal information may be stored and processed in any country where we have facilities or in which we engage service providers, and by using the platform you consent to the transfer of your personal information to countries outside of your country of residence, including the United States, which may have different data protection rules from those of your country. In certain circumstances, courts, law enforcement agencies, regulatory agencies, or security authorities in those other countries may be entitled to access your personal information.</p>
<p>Some of the non-European Economic Area (&ldquo;EEA&rdquo;) countries are recognised by the European Commission as providing an adequate level of data protection according to EEA standards, and the full list of these countries is available&nbsp;<a class="sc-cSHVUG ZqwWI" href="https://ec.europa.eu/info/law/law-topic/data-protection/data-transfers-outside-eu/adequacy-protection-personal-data-non-eu-countries_en">here</a>.&nbsp;&nbsp;For transfers from the EEA to countries not considered adequate by the European Commission, we have put in place adequate measures, such as standard contractual clauses adopted by the European Commission to protect your personal information. You may obtain a copy of these measures by contacting our Data Protection Team using the contact details listed at the end of this&nbsp;<a class="sc-cSHVUG ZqwWI" href="https://www.DearSarkar.com/policies/privacy#8">Privacy Policy</a>.</p>
<h3 id="7"><strong>7. Third-Party Services</strong></h3>
<div class="box bg-default mvs pam type-l">We&rsquo;re not responsible for the privacy practices of third parties linked to from our Platform.</div>
<p>This Privacy Policy does not address, and we are not responsible for, the privacy, information, or other practices of any third parties, including any third party operating any site or service to which our services link. &nbsp;Our inclusion of a link on our services does not imply our endorsement of the linked site or service.</p>
<h3 id="8"><strong>8. Policy updates &amp; contacting us</strong></h3>
<div class="box bg-default mvs pam type-l">This policy may change over time. We&rsquo;ve included here our contact information, but the best way to get in touch with us is through our online Help Centre.</div>
<p>We may change this Privacy Policy. The &ldquo;Effective Date&rdquo; legend at the top of this Privacy Policy indicates when it was last revised. &nbsp;Any changes will become effective when we post the revised Privacy Policy on our platform.</p>

        <h2>Contact Us</h2>

        <p>If you have any questions about these Terms, please contact us.</p>
    </div>
@endsection
