@component('mail::message')

**{{ $reply->author()->name() }}** has commented to this Petition.

@component('mail::panel')
{{ $reply->excerpt(200) }}
@endcomponent

@component('mail::button', ['url' => route('thread', $reply->replyAble()->slug())])
View Petition
@endcomponent

@component('mail::subcopy')
    You are receiving this because you are subscribed to this petition updates.  
    [Unsubscribe]({{ route('subscriptions.unsubscribe', $subscription->uuid()->toString()) }}) from this petition updates.
@endcomponent

@endcomponent
