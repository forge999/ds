@title('Login')

@extends('layouts.small')

@section('small-content')
    <form action="{{ route('login.post') }}" method="POST" class="w-full">
        @csrf

        @formGroup('username')
            <label for="username">Username</label>
            <input type="text" id="username" name="username" required class="form-control" />
            @error('username')
        @endFormGroup

        @formGroup('password')
            <label for="password">Password</label>
            <input type="password" id="password" name="password" required class="form-control" />
            @error('password')
        @endFormGroup

        <div class="form-group">
            <label>
                <input name="remember" type="checkbox" value="1">
                Remember login
            </label>
        </div>
        
        <button type="submit" class="w-full button button-primary mb-4">Login</button>



        <a href="{{ route('login.github') }}" class="button button-dark mb-4">
            <i class="fa fa-github mr-1"></i> Github
        </a>
        <a href="{{ route('login.twitter') }}" class="button button-dark mb-4">
            <i class="fa fa-twitter mr-1"></i> Twitter
        </a>

        <a href="{{ route('login.google') }}" class="button button-dark mb-4">
            <i class="fa fa-google mr-1"></i> Google
        </a>
        <a href="{{ route('login.facebook') }}" class="button button-dark mb-4">
            <i class="fa fa-facebook mr-1"></i> Facebook
        </a>
    </form>
@endsection

@section('small-content-after')
    <a href="{{ route('password.forgot') }}" class="block text-center text-green-darker">Forgot your password?</a>
@endsection
