<?php

return [

    'created' => 'Comment successfully added!',
    'updated' => 'Comment successfully updated!',
    'deleted' => 'Comment successfully deleted!',

];
