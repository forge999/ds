<?php

return [

    'threads.created' => 'Petition successfully created!',
    'threads.updated' => 'Petition successfully updated!',
    'threads.deleted' => 'Petition successfully deleted!',

];
