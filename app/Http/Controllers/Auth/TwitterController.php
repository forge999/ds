<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Jobs\UpdateProfile;
use App\Social\TwitterUser;
use App\User;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\RedirectResponse;
use Laravel\Socialite\One\InvalidStateException;
use Laravel\Socialite\One\User as SocialiteUser;
use Socialite;

class TwitterController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     */
    public function redirectToProvider()
    {
        return Socialite::driver('twitter')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     */
    public function handleProviderCallback()
    {
        try {
            $socialiteUser = $this->getSocialiteUser();
        } catch (InvalidStateException $exception) {
            $this->error('errors.github_invalid_state');

            return redirect()->route('login');
        }

        try {
            // $user = User::findByGithubId($socialiteUser->getId());
            $user = User::findByEmailAddress($socialiteUser->getEmail());
        } catch (ModelNotFoundException $exception) {
            
            $twUser = [
                "id" => $socialiteUser->id,
                "name" => $socialiteUser->name,
                "email" => $socialiteUser->email,
                "nickname" => $socialiteUser->nickname,
                "created_at" => $socialiteUser->user["created_at"]
            ];
            
            return $this->userNotFound(new TwitterUser($twUser));
        }

        return $this->userFound($user, $socialiteUser);
    }

    private function getSocialiteUser(): SocialiteUser
    {
        return Socialite::driver('twitter')->user();
    }

    private function userFound(User $user, SocialiteUser $socialiteUser): RedirectResponse
    {
        $this->dispatchNow(new UpdateProfile($user, ['github_username' => $socialiteUser->getNickname()]));

        Auth::login($user);

        return redirect()->route('dashboard');
    }

    private function userNotFound(TwitterUser $user): RedirectResponse
    {
        if ($user->isTooYoung()) {
            $this->error('errors.github_account_too_young');

            return redirect()->home();
        }

        return $this->redirectUserToRegistrationPage($user);
    }

    private function redirectUserToRegistrationPage(TwitterUser $user): RedirectResponse
    {
        session(['githubData' => $user->toArray()]);
        return redirect()->route('register');
    }
}
